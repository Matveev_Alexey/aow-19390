#!/usr/bin/env python3

import argparse
from pathlib import Path
import errno
import zlib
import os
import json
import yaml


def calc_crc(file_name):
    prev = 0
    for each_line in open(os.path.abspath(file_name), "rb"):
        prev = zlib.crc32(each_line, prev)
    return "%X" % (prev & 0xFFFFFFFF)


def get_file_info(file, init_dir):
    file_info = {}
    absolute_file_path = os.path.abspath(file)
    file_info["filepath"] = absolute_file_path[len(init_dir) + 1:]
    crc = calc_crc(file)
    file_info["crc"] = crc
    file_stats = os.stat(file)
    file_info["size"] = file_stats.st_size
    return file_info


parser = argparse.ArgumentParser(description='Python meta generator')
parser.add_argument('-r', '--recursive', action='store_true')
parser.add_argument('-d', '--directory', required=True)
parser.add_argument('-e', '--extension', nargs='+', required=True)
parser.add_argument('-c', '--cdn-path', required=True)
parser.add_argument('-o', '--out-file', required=True)
parser.add_argument('-t', '--type')

args = parser.parse_args()

if not os.path.isdir(args.directory):
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.directory)

absolute_dir_path = os.path.abspath(args.directory)
recursive = args.recursive

print(f"Start generate meta in {absolute_dir_path} recursive = {recursive}")
files = []

for extension in args.extension:
    if recursive:
        for path in Path(args.directory).rglob(f"*.{extension}"):
            files.append(path)
    else:
        for path in Path(args.directory).glob(f"*.{extension}"):
            files.append(path)

files_info = []

for file in files:
    files_info.append(get_file_info(file, absolute_dir_path))

if not files_info:
    raise ValueError('No files pattern match found.')

if args.type == "yaml":
    dictionary = {
        "path": args.cdn_path,
        "files": files_info
    }
    content = yaml.dump(dictionary)
else:
    dictionary = {
        "path": args.cdn_path,
        "files": files_info
    }
    content = json.dumps(dictionary, indent=4)
out_file = open(args.out_file, "w")
out_file.write(content)
out_file.close()
print(f"Meta generated at {args.out_file}")
