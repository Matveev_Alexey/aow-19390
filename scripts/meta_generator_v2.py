import re
from consul_kv import Connection
import argparse
from pathlib import Path
import errno
import zlib
import os
import yaml
import logging
import boto3
from botocore.exceptions import ClientError
from gg_vault import Vault


def vault(vault_login, vault_pass):
    vault_engine = Vault('aow')
    vault_engine.login(vault_login=vault_login, vault_pass=vault_pass)
    secret = vault_engine.get_secret('private/s3')
    return secret


def selectel(access_key_id, secret_access_key):
    return boto3.client(
        service_name='s3',
        endpoint_url='https://s3.storage.selcloud.ru',
        region_name='ru-1',
        aws_access_key_id=access_key_id,
        aws_secret_access_key=secret_access_key
    )


def amazon(access_key_id, secret_access_key):
    return boto3.client(
        service_name='s3',
        region_name='eu-central-1',
        aws_access_key_id=access_key_id,
        aws_secret_access_key=secret_access_key
    )


def calc_crc(file_name):
    prev = 0
    for each_line in open(os.path.abspath(file_name), "rb"):
        prev = zlib.crc32(each_line, prev)
    return "%X" % (prev & 0xFFFFFFFF)


def get_file_info(file, init_dir):
    file_info = {}
    absolute_file_path = os.path.abspath(file)
    file_info["filepath"] = absolute_file_path[len(init_dir) + 1:]
    crc = calc_crc(file)
    file_info["fileName"] = file_info['filepath']
    file_info["crc"] = crc
    return file_info


def remove_hash(file_name):
    file_type = file_name.split('.')[-1]
    _name = '_'.join(file_name.split('_')[0:-1])
    file = re.findall('([0-9]{0,}_[0-9]{0,}_[0-9]{0,})(.)(.*)', _name)[0][-1]
    return f"{file}.{file_type}"


def add_hash(file_name, hash):
    info = file_name.split('.')
    return f"{info[0]}_{hash}.{info[1]}"



parser = argparse.ArgumentParser(description='Python meta generator')
parser.add_argument('-d', '--directory', required=True)
parser.add_argument('-e', '--extension', nargs='+', required=True)
parser.add_argument('-c', '--cdn_path', required=True)
parser.add_argument('--hash', required=True)
parser.add_argument('--consul_ip', required=True)
parser.add_argument('--consul_key_path', required=True)
parser.add_argument('--git_diff', required=True)
parser.add_argument('--force', action='store_true', default=False)
parser.add_argument('--consul_meta', required=True)
parser.add_argument('-b', '--bucket', required=True)
parser.add_argument('--vault_login', required=True)
parser.add_argument('--vault_pass', required=True)

args = parser.parse_args()

if not os.path.isdir(args.directory):
    raise FileNotFoundError(errno.ENOENT, os.strerror(errno.ENOENT), args.directory)

absolute_dir_path = os.path.abspath(args.directory)
conn = Connection(endpoint=args.consul_ip)
consul_key_path = conn.get(args.consul_key_path)
vault_secrets = vault(args.vault_login, args.vault_pass)
s3 = selectel(vault_secrets['selectel_access_key_id'], vault_secrets['selectel_secret_access_key'])
a3 = amazon(vault_secrets['amazon_access_key_id'], vault_secrets['amazon_secret_access_key'])



print(f"Start generate meta in {absolute_dir_path}")

if args.force:
    files = []

    for extension in args.extension:
        for path in Path(args.directory).rglob(f"*.{extension}"):
            files.append(path)
    files_info = []

    for file in files:
        files_info.append(get_file_info(file, absolute_dir_path))
    if not files_info:
        raise ValueError('No files pattern match found.')

    for i in files_info:
        file_path_local = i['filepath']
        i['filepath'] = add_hash(i['filepath'], i['crc'])
        try:
            path_local = f"{absolute_dir_path}/{file_path_local}"
            new_file = i['filepath']
            s3.upload_file(path_local, args.bucket, f"{args.cdn_path}/{i['filepath']}")
            a3.upload_file(path_local, args.bucket, f"{args.cdn_path}/{i['filepath']}")
            print(f"upload file {i['filepath']} to {args.cdn_path}")
        except ClientError as e:
            logging.error(e)
            print(f"File {i['filepath']} to {args.cdn_path} NOT UPLOAD")
        except:
            print(f"File {i['filepath']} to {args.cdn_path} NOT UPLOAD")
    for cdn_path in files_info:
        cdn_path['filepath'] = f"{args.cdn_path}/{cdn_path['filepath']}"
    result = {
        args.consul_meta: {
            'files': files_info
        }
    }
    conn.put(args.consul_key_path, yaml.dump(result))
    print("change value in consul")

if not args.force:
    manifest = consul_key_path[f'{args.consul_key_path}']
    changes_files = args.git_diff.split()
    manifest_parsed = yaml.safe_load(manifest)

    changes = []
    for files in changes_files:
        i = re.findall('^Prefabs/[\\w\\D]*', files)
        if i:
            changes += i

    file_info = {}
    for change in changes:
        try:
            get_info = get_file_info(Path(f"../{change}"), absolute_dir_path)
            file_info[get_info['filepath']] = get_info
        except FileNotFoundError:
            file_info[change.replace('Prefabs/', '')] = {'filepath': change.replace('Prefabs/', ''), 'delete': True}

    if not file_info:
        raise ValueError('No files pattern match found.')
    manifest_files = []
    for file in manifest_parsed[args.consul_meta]['files']:
        filepath = remove_hash(file['filepath'])
        list_filepath = list(file_info.keys())
        if filepath not in list(file_info.keys()):
            manifest_files.append(file)
        elif file_info[filepath].get('delete', False):
            print(file_info[filepath])
            continue
        else:
            try:
                path_local = f"{absolute_dir_path}/{filepath}"
                file_info[filepath]['filepath'] = f"{args.cdn_path}/{add_hash(filepath, file_info[filepath]['crc'])}"
                s3.upload_file(path_local, args.bucket, file_info[filepath]['filepath'])
                a3.upload_file(path_local, args.bucket, file_info[filepath]['filepath'])
                print(f"upload file {file_info[filepath]['filepath']}")
            except ClientError as e:
                logging.error(e)
                print(f"File {file_info[filepath]['filepath']} NOT UPLOAD")
            except:
                print(f"File {file_info[filepath]['filepath']} NOT UPLOAD")
            manifest_files.append(file_info[filepath])
    if args.extension == ['bytes']:
        result = {
            args.consul_meta: {
                'files': manifest_files
            }
        }
    else:
        result = {
            args.consul_meta: {
                'files': manifest_files,
                'zipPath': manifest_parsed[f'{args.consul_meta}']['zipPath']
            }
        }

    consul_push = yaml.dump(result)
    print(consul_push)
    conn.put(args.consul_key_path, consul_push)
    print("upload to consul")
